# TEMPLATE

This folder is the template with the latest configurations. When creating a new VM simply copy this template with the version. When the template is updated simply increase the version.

## Hierarchy

The template is organised as followed:

```bash
template
├──README.md
├──providers.tf
└──vm-conf.tf
```

### README.md

The README starts with the title written in capital letters and named after the VMs it contains without a number at the end (example: T3MARIADB).\
A short description of the VM and the following informations are provided:\
OS: XXXXX \
IP: YYY.YYY.YYY.YYY 
Template version: YYYYMMDD

### vm-conf.tf

This file contains the variable definition of the VM to be created.

\
\
\
Template version: 20211021

### providers.tf

This file contains the libvirt provider and the uri for each KVM server.
