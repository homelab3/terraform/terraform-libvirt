module "t1template1" {
  source       = "git@gitlab.com:homelab3/terraform/terraform-libvirt.git"
  providers = {
    libvirt = libvirt.p1kvm-02
  }
  template  = "/home/didier/Downloads/focal-server-cloudimg-amd64.img" 
  cpu_nb       = "2"
  ram       = "4096"
  name      = "t1template1"
  hostname  = "t1template1"
  network   = "br0"
  pool      = "default"
  ip_v4     = "192.168.1.253"
  gateway   = "192.168.1.1"
  dns       = "192.168.1.2,1.1.1.1"
  sda_size = "30"
  disks = {
    vm_disk_1 = {
      name = "vdb"
      size = 50
    },
    vm_disk_2 = {
      name = "vdc"
      size = 50
    }
  }
}
